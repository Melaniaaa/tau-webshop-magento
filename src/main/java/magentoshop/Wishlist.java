package magentoshop;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;

public class Wishlist extends ScreenShooter {

    private final SelenideElement addToWishlist = $("a.link-wishlist");
    private final SelenideElement moveToWishlist =
            $(".product-cart-actions .cart-links a.link-wishlist.use-ajax");
    private final SelenideElement textArea = $(".wishlist-cell1.customer-wishlist-item-info textarea");
    private final SelenideElement updateWishlist = $(".item-manage .button.btn-update.button-secondary");
    private final SelenideElement deleteWishlist =
            $(".customer-wishlist-item-remove.last .btn-remove.btn-remove2");

    /**
     * Validators
     */
    @Step("Verify if Wishlist is visible and has text")
    public void isAddToWishlistLinkVisibleAndHasText() {
        addToWishlist.shouldBe(visible).shouldBe(enabled);
        addToWishlist.shouldHave(exactText("Add to Wishlist"));
    }

    @Step("Verify if MyCart Move to Wishlist is visible and has text")
    public void isMoveToWishlistVisibleAndHasText() {
        moveToWishlist.shouldBe(visible).shouldBe(enabled);
        moveToWishlist.shouldHave(exactText("Move to wishlist"));
    }

    @Step("Verify if Wishlist Text area is visible")
    public void isTextAreaVisible() {
        textArea.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify if Update Wishlist button is visible and has title")
    public void isUpdateWishlistVisibleAndHasTitle() {
        updateWishlist.shouldBe(visible).shouldBe(enabled);
        updateWishlist.shouldHave(attribute("title", "Update Wishlist"));
    }

    @Step("Verify if Wishlist Delete button is visible and has title")
    public void isDeleteWishlistButtonVisibleAndHasTitle() {
        deleteWishlist.shouldBe(visible).shouldBe(enabled);
        deleteWishlist.shouldHave(attribute("title", "Remove Item"));
    }

    /**
     * Actions
     */

    @Step("Click on Wishlist fron Product page")
    public void clickOnAddToWishlistLink() {
        addToWishlist.hover().click();
        takeScreenShot("Click on Wishlist fron Product page");
    }

    @Step("Click on Move to Wishlist fron Cart page")
    public void clickOnMoveToWishlistLink() {
        moveToWishlist.hover().click();
        takeScreenShot("Click on Move to Wishlist fron Cart page");
    }

    @Step("Fill in Wishlist Text area")
    public void fillInTextArea (String input) {
        textArea.clear();
        textArea.sendKeys(input);
    }

    @Step("Click on Update Wishlist button")
    public void clickOnUpdateWishlistButton() {
        updateWishlist.hover().click();
    }

    @Step("Click on Wishlist Delete button")
    public void clickOnDeleteWishlistButton() {
        deleteWishlist.hover().click();
        Selenide.switchTo().alert().accept();
    }
}
