package magentoshop;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

public class Register extends ScreenShooter {

   private SelenideElement registerPageTitle= $(".account-create .page-title h1");
    // se poate folosi si selectorul mai simplu $(".page-title h1");
   private SelenideElement registerFormInstructions = $(".account-create .fieldset .form-instructions");
    // se poate folosi si numia selectorul mai simplu $(".form-instructions");
   private SelenideElement registerRequiredFields = $(".account-create .fieldset p.required");
    // se poate folosi si selectorul simplu $("p.required");
   private SelenideElement firstNameInput = $("input#firstname");
   private SelenideElement middleNameInput = $("input#middlename");
   private SelenideElement lastNameInput = $("input#lastname");
   private SelenideElement registerEmailAddressInput = $("input#email_address");
   private SelenideElement registerPasswordInput = $("input#password");
   private SelenideElement registerConfirmPasswordInput = $("input#confirmation");
   private SelenideElement subscribedInput = $("input#is_subscribed.checkbox");
   private SelenideElement registerBackLink = $("a.back-link");
   private SelenideElement registerButton = $(".buttons-set [type=submit]");
   private SelenideElement successMessage = $(".success-msg span");
   private SelenideElement errorMessage = $(".error-msg span");
   private final SelenideElement welcomeMessage = $(".welcome-msg");
   private SelenideElement validationBlankFirstName = $("#advice-required-entry-firstname.validation-advice");
   private SelenideElement validationBlankLastName = $("#advice-required-entry-lastname.validation-advice");
   private SelenideElement validationBlankEmailAddress = $("#advice-required-entry-email_address.validation-advice");
   private SelenideElement validationBlankPassword = $("#advice-required-entry-password.validation-advice");
   private SelenideElement validationBlankConfirmPassword = $("#advice-required-entry-confirmation.validation-advice");
   private SelenideElement validationConfirmPassword = $("#advice-validate-cpassword-confirmation.validation-advice");

    /**
     * Validators
     */

    @Step("Verify if Register page is visible and has title")
    public void isRegisterPageVisibleAndHasTitle() {
        registerPageTitle.shouldBe(visible).shouldHave(exactText("Create an Account"));
        takeScreenShot("Register page is visible and has title");
    }

    @Step("Verify if Register Form Instructions is visible and has text")
    public void isRegisterFormInstructionsVisibleAndHasText() {
        registerFormInstructions.shouldBe(visible)
                .shouldHave(exactText("Please enter the following information to create your account."));
        takeScreenShot("Register Form Instructions is visible and has text");
    }

    public void isRegisterReguiredFieldsVisibleAndHasText() {
        registerRequiredFields.shouldBe(visible).shouldHave(exactText("* Required Fields"));
    }

    @Step("Verify if Register First Name is visible")
    public void isFirstNameVisible() {
        firstNameInput.shouldBe(visible);
    }

    @Step("Verify if Register First Name is required field and has title")
    public void isFirstNameRequiredFieldAndHasTitle() {
        firstNameInput.shouldHave(cssClass("required-entry"));
        firstNameInput.shouldHave(attribute("title","First Name"));
        takeScreenShot("First Name is required field and has title");
    }

    @Step("Verify if Register Middle Name is visible")
    public void isMiddleNameVisible() {
        middleNameInput.shouldBe(visible);
    }

    @Step("Verify if Register Middle Name is required field and has title")
    public void isMiddleNameNotRequiredFieldAndHasTitle() {
        middleNameInput.click();
        middleNameInput.shouldNotHave(cssClass("required-entry"));
        //pt test negativ
        //middleNameInput.shouldHave(cssClass("required-entry"));
        middleNameInput.shouldHave(attribute("title","Middle Name/Initial"));
    }

    @Step("Verify if Register Last Name is visible")
    public void isLastNameVisible() {
        lastNameInput.shouldBe(visible);
    }

    @Step("Verify if Register Last Name is required field and has title")
    public void isLastNameRequiredFieldAndHasTitle() {
        lastNameInput.click();
        lastNameInput.shouldHave(cssClass("required-entry"));
        lastNameInput.shouldHave(attribute("title","Last Name"));
        takeScreenShot("Register Last Name is required field and has title");
    }

    @Step("Verify if Register Email Address is visible")
    public void isRegisterEmailAddressInputVisible() {
        registerEmailAddressInput.shouldBe(visible);
    }

    @Step("Verify if Register Email Address is required field and has title")
    public void isRegisterEmailAddressInputRequiredFieldAndHasTitle() {
        registerEmailAddressInput.click();
        registerEmailAddressInput.shouldHave(cssClass("required-entry"));
        registerEmailAddressInput.shouldHave(attribute("title","Email Address"));
        takeScreenShot("Register Email Address is required field and has title");
    }

    @Step("Verify if Register Password is visible")
    public void isRegisterPasswordInputVisible() {
        registerPasswordInput.shouldBe(visible);
    }

    @Step("Verify if Register Password is required field and has title")
    public void isRegisterPasswordInputRequiredFieldAndHasTitle() {
        registerPasswordInput.click();
        registerPasswordInput.shouldHave(cssClass("required-entry"));
        registerPasswordInput.shouldHave(attribute("title","Password"));
        takeScreenShot("Register Password is required field and has title");
    }

    @Step("Verify if Register Confirm Password is visible")
    public void isRegisterConfirmPasswordInputVisible() {
        registerConfirmPasswordInput.shouldBe(visible);
    }

    @Step("Verify if Register Confirm Password is required field and has title")
    public void isRegisterConfirmPasswordInputRequiredFieldAndHasTitle() {
        registerConfirmPasswordInput.click();
        registerConfirmPasswordInput.shouldHave(cssClass("required-entry"));
      //  registerConfirmPasswordInput.shouldHave(pseudo(":after","*"));
        registerConfirmPasswordInput.shouldHave(attribute("title","Confirm Password"));
        takeScreenShot("Register Confirm Password is required field and has title");
    }

    @Step("Verify if Register Subscribed Input is visible")
    public void isSubscribedInputVisible() {
        subscribedInput.shouldBe(visible);
    }

    @Step("Verify if Register Subscribed Input has type checkbox")
    public void isSubscribedInputCheckbox() {
        subscribedInput.shouldHave(type("checkbox"));
    }

    @Step("Verify if Register Subscribed Input has title")
    public void isSubscribedInputHasTitle() {
        subscribedInput.shouldHave(attribute("title","Sign Up for Newsletter"));
    }

    @Step("Verify if Register Back link is visible")
    public void isRegisterBackLinkVisible() {
        registerBackLink.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify if Register button is visible")
    public void isRegisterButtonVisible() {
        registerButton.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify if Register button has title")
    public void isRegisterButtonHasTitle() {
        registerButton.hover();
        registerButton.shouldHave(attribute("title","Register"));
        takeScreenShot("Register button has title");
    }

    @Step("Verify if Register Success Message is visible and has text")
    public void isSuccessMessageVisibleAndHasText() {
        successMessage.shouldBe(visible).shouldHave(exactText("Thank you for registering with Madison Island."));
        takeScreenShot("Register Success Message visible and has text");
    }

    @Step("Verify if Register Error Message is visible and has text")
    public void isErrorMessageVisibleAndHasText() {
        errorMessage.shouldBe(visible).shouldHave(text("There is already an account with this email address. " +
                "If you are sure that it is your email address, click here to get your password and access" +
                " your account."));
        takeScreenShot("Register Error Message visible and has text");
    }

    @Step("Verify if Register Validation Message First Name Blank is visible and has text")
    public void isValidationMessageFirstNameBlankVisibleAndHasText() {
        validationBlankFirstName.shouldBe(visible).shouldHave(exactText("This is a required field."));
        takeScreenShot("Register Validation Message First Name Blank is visible and has text");
    }

    @Step("Verify if Register Validation Message Last Name Blank is visible and has text")
    public void isValidationMessageLastNameBlankVisibleAndHasText() {
        validationBlankLastName.shouldBe(visible).shouldHave(exactText("This is a required field."));
        takeScreenShot("Register Validation Message Last Name Blank is visible and has text");
    }

    @Step("Verify if Register Validation Message Email Address Blank is visible and has text")
    public void isValidationMessageEmailAddressBlankVisibleAndHasText() {
      validationBlankEmailAddress.shouldBe(visible).shouldHave(exactText("This is a required field."));
      takeScreenShot("Register Validation Message Email Address Blank is visible and has text");
    }

    @Step("Verify if Register Validation Message Password Blank is visible and has text")
    public void isValidationMessagePasswordBlankVisibleAndHasText() {
       validationBlankPassword.shouldBe(visible).shouldHave(exactText("This is a required field."));
       takeScreenShot("Register Validation Message Password Blank is visible and has text");
    }

    @Step("Verify if Register Validation Message Confirm Password Blank is visible and has text")
    public void isValidationMessageConfirmPasswordBlankVisibleAndHasText() {
        validationBlankConfirmPassword.shouldBe(visible).shouldHave(exactText("This is a required field."));
        takeScreenShot("Register Validation Message Confirm Password Blank is visible and has text");
    }

    @Step("Verify if Register Validation Message Confirm Password is visible and has text")
    public void isValidationMessageConfirmPasswordVisibleAndHasText() {
        validationConfirmPassword.shouldBe(visible).shouldHave(exactText("Please make sure your passwords match."));
        takeScreenShot("Register Validation Message Confirm Password is visible and has text");
    }

    /**
     * Actions
     */

    @Step("Fill in Register First Name")
    public void fillInFirstName(String input){
        firstNameInput.sendKeys(input);
        takeScreenShot("Fill in Register First Name");
    }

    @Step("Fill in Register Middle Name")
    public void fillInMiddleName(String input){
        middleNameInput.sendKeys(input);
        takeScreenShot("Fill in Register Middle Name");
    }

    @Step("Fill in Register Last Name")
    public void fillInLastName(String input){
        lastNameInput.sendKeys(input);
        takeScreenShot("Fill in Register Last Name");
    }

    @Step("Fill in Register Email Address")
    public void fillInRegisterEmailAddress(String input){
        registerEmailAddressInput.sendKeys(input);
        takeScreenShot("Fill in Register Email Address");
    }

    @Step("Fill in Register Password")
    public void fillInRegisterPassword(String input){
        registerPasswordInput.sendKeys(input);
        takeScreenShot("Fill in Register Password");
    }

    @Step("Fill in Register Confirm Password")
    public void fillInRegisterConfirmPassword(String input){
        registerConfirmPasswordInput.sendKeys(input);
        takeScreenShot("Fill in Register Confirm Password");
    }

    @Step("Click on Register button")
    public void clickOnRegisterButton() {
        registerButton.click();
    }

    @Step("Click on Register Back link")
    public void clickOnRegisterBackLink() {
        registerBackLink.hover().click();
    }

    @Step("Select Subscribed CheckBox")
    public void selectSubscribedCheckBox() {
        subscribedInput.hover().click();
        takeScreenShot("Select Subscribed CheckBox");
    }
}
