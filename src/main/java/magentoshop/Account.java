package magentoshop;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

public class Account extends ScreenShooter {

    private final SelenideElement myAccount = $("#header-account .first a[href*='/account']");
    //$$("#header-account .first>a[href*='/account']")
    private final SelenideElement myWhislist = $("#header-account a[href*='/wishlist']");
    private final SelenideElement myCart = $("#header-account a.top-link-cart");
    private final SelenideElement checkout = $("#header-account a.top-link-checkout");
    private final SelenideElement login = $("#header-account .last a[href*='/login']");
    private final SelenideElement logout = $("#header-account .last a[href*='/logout']");
    private final SelenideElement register = $("#header-account a[href*='/create']");


    /**
     * Verifiers
     */
    @Step("Verify if MyAccount is visible and enable")
    public void isMyAccountVisibleAndHasTitleText() {
        myAccount.shouldBe(visible).shouldBe(enabled);
        myAccount.shouldHave(attribute("title", "My Account"));
        takeScreenShot("My Account is visible");
    }

    @Step("Verify if MyWhislist option is visible and enable")
    public void isMyWhislistVisibleAndHasTitleText() {
        myWhislist.shouldBe(visible).shouldBe(enabled);
        myWhislist.shouldHave(attribute("title", "My Wishlist"));
        takeScreenShot("My Whislist is visible");
    }

    @Step("Verify if MyCart option is visible and enable")
    public void isMyCartVisibleAndHasTitleText() {
        myCart.shouldBe(visible).shouldBe(enabled);
        myCart.shouldHave(attribute("title","My Cart"));
        //myCart.shouldHave(text("My Cart"));
        takeScreenShot("My Cart is visible");
    }

    @Step("Verify if Checkout option is visible and enable")
    public void isCheckoutVisibleAndHasText() {
        checkout.shouldBe(visible).shouldBe(enabled);
        checkout.shouldHave(text("Checkout"));
        //takeScreenShot("Checkout is visible");
    }

    @Step("Verify if Login option is visible and enable")
    public void isLoginVisibleAndHasText() {
        login.shouldBe(visible).shouldBe(enabled).shouldHave(text("Log In"));
        //takeScreenShot("Login is visible");
    }

    @Step("Verify if Logout option is visible and enable")
    public void isLogoutVisibleAndHasText() {
        logout.shouldBe(visible).shouldBe(enabled).shouldHave(text("Log Out"));
        //takeScreenShot("Logout is visible");
    }

    @Step("Verify if Register option is visible and enable")
    public void isRegisterVisibleAndHasTitle() {
        register.shouldBe(visible).shouldBe(enabled)
                .shouldHave(attribute("title","Register"));
        takeScreenShot("Register is visible");
    }

    /**
     * Actions
     */

    @Step("Open MyAccount link option")
    public void openMyAccount() {
        myAccount.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open MyAccount link option");
    }

    @Step("Open Whislist link option")
    public void openMyWhislist() {
        myWhislist.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open Whislist link option");
    }

    @Step("Open MyCart link option")
    public void openMyCart() {
        myCart.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open MyCart link option");
    }

    @Step("Open Checkout link option")
    public void openCheckout() {
        checkout.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open Checkout link option");
    }

    @Step("Open Login link option")
    public void openLogin() {
        login.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open Login link option");
    }

    @Step("Open Register link option")
    public void openRegister() {
        register.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open Register link option");
    }

    @Step("Open LogOut link option")
    public void clickLogOut() {
        logout.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Open Register link option");
    }
}