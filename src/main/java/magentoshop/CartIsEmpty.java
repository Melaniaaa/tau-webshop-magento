package magentoshop;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

public class CartIsEmpty extends ScreenShooter {
    private final SelenideElement emptyCart = $(".page-title h1");
    private final SelenideElement emptyFirstMessage = $(".cart-empty p:first-child");
    private final SelenideElement emptySecondMessage = $(".cart-empty p a");

    @Step("Verify Page Title when Cart Is Empty")
    public void verifyPageTitleWhenCartIsEmpty(String pageTitle) {
        emptyCart.shouldBe(visible).shouldHave(text(pageTitle));
        takeScreenShot("Page Title when Cart is empty");
    }

    @Step("Verify First Message when Cart Is Empty")
    public void verifyFirstMessageWhenCartIsEmpty(String firstMessage) {
        emptyFirstMessage.shouldBe(visible).shouldHave(text(firstMessage));
        takeScreenShot("First Messages when Cart is empty");
    }

    @Step("Verify Second Message when Cart Is Empty")
    public void verifySecondMessageWhenCartIsEmpty(String secondMessage) {
        emptySecondMessage.shouldBe(visible).shouldHave(text(secondMessage));
        takeScreenShot("Second Messages when Cart is empty");
    }

    /**
     * Actions
     */

    @Step("Continue Shopping when Cart Is Empty")
    public void continueShoppingWhenCartIsEmpty() {
        emptySecondMessage.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Continue Shopping when Cart is empty");
    }
}
