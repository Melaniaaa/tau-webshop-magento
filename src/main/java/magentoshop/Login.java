package magentoshop;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Login extends ScreenShooter {
    private SelenideElement accountLogin = $("account-login");
    private SelenideElement accountloginText = $(".account-login>.page-title h1");
    private SelenideElement newUsers = $(".col-1.new-users");
    private final SelenideElement createAnAccountButton = $(".buttons-set a[href*='/account/create']");
                                                         // $(".buttons-set a.button")
    private SelenideElement loginContents = $(".content.fieldset");
    private SelenideElement emaiAddresslInput = $(".content.fieldset .input-box input#email");
                                       // $("input#email.input-text.required-entry.validate-email");
    private SelenideElement passwordInput = $(".content.fieldset .input-box input#pass");
    private SelenideElement forgotPassword = $(".content.fieldset a.f-left");
    private SelenideElement loginButton = $(".buttons-set [type=submit]");
    private SelenideElement backToLogin = $(".back-link a");
    private SelenideElement errorMessagePassword = $(".error-msg span");
    private SelenideElement errorMessageEmail = $("#advice-validate-email-email.validation-advice");
    private SelenideElement validationBlankEmailAddress = $("#advice-required-entry-email.validation-advice");
    private SelenideElement validationBlankPassword = $("#advice-required-entry-pass.validation-advice");
    private SelenideElement myDashboard = $(".dashboard .page-title h1");
    private SelenideElement firstWelcomeMessage = $(".dashboard .welcome-msg p.hello");
    private SelenideElement secondWelcomeMessage= $(".dashboard .welcome-msg :not(p.hello,strong)");

    /**
     * Validators
     */

    @Step("Verify if Login Page visible and has text")
    public void isLoginPageVisibleAndHasLoginText(String loginText) {
       // accountLogin.shouldBe(visible);
        accountloginText.shouldBe(visible).shouldHave(exactText(loginText));
        takeScreenShot("Account Login is visible");
    }

    @Step("Verify Login Page contents")
    public void isNewUsersFromLoginPageVisibleAndHasContents() {
        newUsers.$(".content h2").shouldBe(visible).shouldHave(exactText("New Here?"));
        newUsers.$(".content p").shouldBe(visible).shouldHave(exactText("Registration is free and easy!"));
        newUsers.$(".content li:nth-child(1)").shouldBe(visible).shouldHave(exactText("Faster checkout"));
        newUsers.$(".content li:nth-child(2)").shouldBe(visible).shouldHave(exactText("Save multiple shipping addresses"));
        newUsers.$(".content li:nth-child(3)").shouldBe(visible).shouldHave(exactText("View and track orders and more"));
        takeScreenShot("Login Page contents");
    }

    @Step("Verify Already Register contents")
    public void isAlreadyRegisteredFromLoginPageVisibleAndHasContents() {
        loginContents.$("h2").shouldBe(visible).shouldHave(exactText("Already registered?"));
        loginContents.$(".form-instructions").shouldBe(visible)
                .shouldHave(exactText("If you have an account with us, please log in."));
        loginContents.$(".required").shouldBe(visible).shouldHave(exactText("* Required Fields"));
        emaiAddresslInput.shouldBe(visible).shouldHave(attribute("title","Email Address"));
        passwordInput.shouldBe(visible).shouldHave(attribute("title","Password"));
        forgotPassword.shouldBe(visible).shouldHave(exactText("Forgot Your Password?"));
        loginButton.shouldBe(visible).shouldHave(attribute("title", "Login"));
        takeScreenShot("Already Register contents");
    }

    @Step("Verify Email Address field is visible")
    public void isEmailAddressVisible() {
        emaiAddresslInput.shouldBe(visible);
    }

    @Step("Verify Email Address is required field")
    public void isEmailAddressRequiredField() {
        emaiAddresslInput.shouldHave(cssClass("required-entry"));
    }

    @Step("Verify Password field is visible")
    public void isPasswordVisible() {
        passwordInput.shouldBe(visible);
    }

    @Step("Verify Password is required field")
    public void isPasswordRequiredField() {
        passwordInput.click();
        passwordInput.shouldHave(cssClass("required-entry"));
    }

    @Step("Verify Login button is visible")
    public void isLoginButtonVisible(){
        loginButton.shouldBe(visible);
    }

    @Step("Verify Login button is enable")
    public void isLoginButtonEnable(){
        loginButton.shouldBe(enabled);
    }

    @Step("Verify Forgot Password link is visible")
    public void isForgotPasswordVisible(){
        loginButton.shouldBe(visible);
    }

    @Step("Verify Back to Login link is visible")
    public void isBackToLoginVisible(){
        backToLogin.shouldBe(visible);
    }

    @Step("Verify error message when password is wrong")
    public void wrongPassword(){
        errorMessagePassword.shouldBe(visible);
        errorMessagePassword.shouldHave(text("Invalid login or password."));
        takeScreenShot("Error message when password is wrong");
    }

    @Step("Verify error message when email is wrong")
    public void wrongEmail(){
        errorMessageEmail.shouldBe(visible);
        errorMessageEmail.shouldHave(text("Please enter a valid email address. For example johndoe@domain.com."));
        takeScreenShot("Error message when email address is wrong");
    }

    @Step("Check message displayed when email address is blank")
    public void isValidationBlankEmailAddress(){
        validationBlankEmailAddress.shouldBe(visible);
        validationBlankEmailAddress.shouldHave(text("This is a required field."));
        takeScreenShot("Message displayed when email address is blank");
    }

    @Step("Check message displayed when password is blank")
    public void isValidationBlankPassword(){
        validationBlankPassword.click();
        validationBlankPassword.shouldBe(visible);
        validationBlankPassword.shouldHave(text("This is a required field."));
        takeScreenShot("Message displayed when password is blank");
    }

    @Step("Verify MyDashboard is visible and has text")
    public void isMyDashboardVisibleAndHasText(){
        myDashboard.shouldBe(visible).shouldHave(exactText("MY DASHBOARD"));
    }

    @Step("Verify MyDashboard first welcome message is visible")
    public void isFirstWelcomeMessageVisibleAndHasText(){
       firstWelcomeMessage.shouldBe(visible).shouldHave(text("Hello,"));
    }

    @Step("Verify MyDashboard second message is visible")
    public void isSecondWelcomeMessageVisibleAndHasText(){
        secondWelcomeMessage.shouldBe(visible)
           .shouldHave(exactText("From your My Account Dashboard you have the ability to view a snapshot of your " +
                   "recent account activity and update your account information. Select a link below to view or " +
                   "edit information."));
        takeScreenShot("MyDashboard second message");
    }

    /**
     * Actions
     */

    @Step("Create an Account from Login Page")
    public void createAnAccountFromLoginPage() {
        createAnAccountButton.shouldBe(visible).shouldBe(enabled);
        createAnAccountButton.shouldHave(attribute("title","Create an Account"));
        createAnAccountButton.hover().click();
    }

    @Step("Fill in Email Address field")
    public void fillInEmailAddress(String input){
       emaiAddresslInput.sendKeys(input);
    }

    @Step("Fill in Password field")
    public void fillInPassword(String input){
        passwordInput.sendKeys(input);
    }

    @Step("Click on Login Button")
    public void clickOnLoginButton(){
        takeScreenShot("Login");
        loginButton.hover().scrollTo().click();
    }

    @Step("Click on Forgot Password link")
    public void clickOnForgotPassword(){
       forgotPassword.hover().click();
    }

    @Step("Click on Back to Login link")
    public void clickOnBackToLogin(){
        backToLogin.click();
    }
}
