package magentoshop;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.Boolean.valueOf;

public class MagentoShopBasePage extends ScreenShooter {

    private final SelenideElement welcomeMessage = $(".welcome-msg");
    private final SelenideElement logo =$(".logo img.large");
    private final SelenideElement formLanguage = $(".form-language");
    private final SelenideElement selectLanguageTwo = $("#select-language");
    private final ElementsCollection selectLanguage = $$("#select-language option");
    private final SelenideElement searchInput = $("#search");
    private final SelenideElement searchButton = $(".search-button");
    private final SelenideElement accountList = $("a.skip-account");
    private final SelenideElement miniHeaderCartIcon = $(".header-minicart .skip-cart .icon");
    private final SelenideElement miniHeaderCartLabel = $(".header-minicart .skip-cart .label");
    private final SelenideElement miniHeaderCartCount = $(".header-minicart .skip-cart .count");


    /**
     *This is the constructor of the MagentoShopBasePage class;
     *The @Step annotation is not put / not accepted on the constructor;
     */
    public MagentoShopBasePage () {

        openPage();
    }

    @Step("Open Home Page")
    private void openPage() {
      open ("http://testfasttrackit.info/magento-test/");
      takeScreenShot("Open Base Page from Magento Shop Aplication");
      //  validateHomePagePoints(); se poate pune aici s-au se poate face un test
    }

    /**
     * Verifiers
     */

    @Step("Verify welcome message text")
    public void verifyWelcomeMessageByText(String welcomeText) {
      takeScreenShot("Verify welcome message text is " + welcomeText);
      welcomeMessage.shouldHave(exactText(welcomeText));
    }

    @Step("Verify logo message text")
    public void verifyLogoMessage(String logoText){
        logo.shouldBe(visible);
        logo.shouldHave(attribute("alt",logoText));
        takeScreenShot("Verify logo message text is " + logoText);
    }

    @Step("Verify language text")
    public void verifyLabelFormLanguage(String languageText){
        formLanguage.$("label").shouldBe(visible).shouldHave(text(languageText));
    }

    @Step("Verify Title Account List")
    public void verifyAccountList(){
        accountList.$("span.label").shouldBe(visible).shouldHave(text("account"));
    }

    @Step("Verify Mini Header Cart Icon")
    public void verifyMiniHeaderCart(){
        miniHeaderCartIcon.shouldBe(visible).shouldBe(enabled);
        miniHeaderCartLabel.shouldBe(visible).shouldHave(exactText("Cart"));
        takeScreenShot("Verify Mini Header Cart Icon");
      //  miniHeaderCartCount.shouldBe(visible);
    }

    @Step("Verify if Mini Header Cart Icon Is Empty")
    public boolean verifyMiniHeaderCartIsEmpty() {
        if (!(miniHeaderCartCount.getText().equals("0"))) {
            System.out.println("The cart is not empty!");
            System.out.println("Number of products from cart are : " + miniHeaderCartCount.getText());
            return true;
        }
        else{
            System.out.println("The cart is empty!");
            return false;
        }
    }

    /**
     * Actions
     */
    public void clickOnSearchButton(){
        searchButton.shouldBe(visible).shouldBe(enabled).click();
    }

    @Step("Open Account List")
    public void openAccountList(){
        accountList.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Account option list");
    }

    @Step("Click on Account List")
    public void clickAccountList(){
        accountList.hover().click();
    }

    @Step("Verify first method for select language")
    public void verifySelectLanguage(String language){
        selectLanguage.findBy(text(language)).click();
    }

    @Step("Verify second method for select language")
    public void verifySelectLanguageTwo(String languageTwo){
        selectLanguageTwo.selectOption(languageTwo);
        takeScreenShot("Verify second method for select language");
    }

    @Step("Verify search option")
    public void verifySearch(String inputSearch){
        searchInput.shouldBe(visible).sendKeys(inputSearch);
        searchButton.shouldBe(visible).shouldBe(enabled).click();
        takeScreenShot("Verify search product : " + inputSearch);
    }

    @Step("Click on Mini Header Cart Icon")
    public void clickOnMiniHeaderCart(){
        miniHeaderCartIcon.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Verify second method for select language");
    }

}

