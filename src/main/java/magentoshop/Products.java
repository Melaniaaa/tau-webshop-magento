package magentoshop;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;


public class Products extends ScreenShooter {

    private final ElementsCollection selectAllMenuProducts = $$(".level0 a.level0");
    private final ElementsCollection selectAllSubmenuProducts = $$(".level1 a.level1");
    private final SelenideElement dressRedProduct = $("a.product-image[href*='red-dress']");
    private final SelenideElement dressSakuraProduct = $(".product-name a[href*='sakura-blossom']");
    private final SelenideElement PageTitle = $(".page-title.category-title h1");
    private final SelenideElement dressPageSubTitle = $(".category-description.std");
    private final SelenideElement zoomImage = $(".zoomContainer");
    private final SelenideElement zoomWindowImage = $(".zoomLens");
    private final SelenideElement pearlStrandProduct =
             $(".product-info .product-name a[href*='pearl-strand-necklace']");
    private final SelenideElement pearlStrandProductPrice = $("#product-price-546 span.price");
    private final SelenideElement pearlStrandProductViewDetails = $("a.button[href*='pearl-strand-necklace']");
    private final SelenideElement sortByBottom = $(".toolbar-bottom .sort-by label");
    private final SelenideElement sortSwitch = $(".toolbar-bottom .sort-by a.sort-by-switcher");
    private final SelenideElement sortBySelectOption = $(".toolbar-bottom .sort-by select");
    private final SelenideElement inStock = $(".availability.in-stock span.value");
    private final SelenideElement pearlStrandNecklaceName = $(".product-name .h1");
    private final SelenideElement pearlStrandNecklacePrice = $("#product-price-546 span.price");
    private final SelenideElement pearlStrandNecklaceShortDescription = $(".short-description .std");
    private final ElementsCollection pearlStrandNecklaceTabs = $$(".toggle-tabs li");
    private final SelenideElement pearlStrandNecklaceImage = $(".product-image-gallery");
    private final SelenideElement addToCartButton = $(".add-to-cart-buttons button.button.btn-cart");
    private final SelenideElement validateRequiredFields = $("#advice-required-entry-attribute209");
    private final SelenideElement chooseAnOption = $("#attribute209");
    private final SelenideElement labelRequired = $("label.required");
    private final SelenideElement goToJewelryPage = $(".category19 a");
    private final SelenideElement goToAccessoriesPage = $(".category6 a");
    private final SelenideElement productsQuantity= $("#qty");
    private final SelenideElement quantityErrorMessage = $(".error-msg span");
    private final ElementsCollection productSubMenuList = $$(".products-grid li.item.last a.product-image");
    private final SelenideElement goToHomePage = $(".home a");
    private final SelenideElement shirtColor = $(".swatch-label img[src*='charcoal']");
    private final SelenideElement shirtSize = $("#swatch78");

//--------------
   private final SelenideElement menuAccessories = $("a[href$='accessories.html'].level0.has-children");
   private final SelenideElement subMenuJewelry = $("a[href$='jewelry.html'].level1");

    /**
     * Validators
     */

    @Step("Verify if Dress page is visible and has title")
    public void isDressPageVisibleAndHasTitle() {
        PageTitle.shouldBe(visible).shouldBe(enabled).shouldHave(exactText("Dress"));
        dressPageSubTitle.shouldBe(visible).shouldBe(enabled).shouldHave(exactText("Only dress"));
        takeScreenShot("Dress page is visible and has title");
    }

    @Step("Verify if Dress products visible and has title")
    public void isDressProductsVisibleAndHasTitle() {
        dressRedProduct.shouldBe(visible).shouldBe(enabled).
                shouldHave(attribute("title", "Red Dress"));
        dressSakuraProduct.shouldBe(visible).shouldBe(enabled).
                shouldHave(attribute("title", "Sakura blossom flower dress , " +
                        "inspirat from anime "));
        takeScreenShot("Dress products is visible and has title");
    }

    @Step("Verify if Jewelry page is visible and has title")
    public void isJewelryPageVisibleAndHasTitle() {
        PageTitle.shouldBe(visible).shouldBe(enabled).shouldHave(exactText("Jewelry"));
        takeScreenShot("Jewelry page is visible and has title");
    }

    @Step("Verify if Pearl Strand product is visible and has title")
    public void isPearlStrandProductVisibleAndHasTextAndPrice() {
        pearlStrandProduct.shouldBe(visible).shouldBe(enabled).shouldHave(exactText("Pearl Strand Necklace"));
        pearlStrandProductPrice.shouldBe(visible).shouldHave(text("250,00 LEU"));
        takeScreenShot("Pearl Strand product is visible and has title");
    }

    @Step("Verify if Pearl Strand view details is visible and has title")
    public void isPearlStrandProductViewDetailsVisibleAndHasTitle() {
        pearlStrandProductViewDetails.shouldBe(visible).
                shouldBe(enabled).shouldHave(attribute("title", "View Details"));
        takeScreenShot("Pearl Strand view details is visible and has title");
    }

    @Step("Verify if Jewelry page SortBy is visible and has text")
    public void isJewelryPageSortByVisibleAndHasText() {
        sortByBottom.shouldBe(visible).shouldBe(enabled).shouldHave(exactText("Sort By"));
        takeScreenShot("Jewelry page SortBy is visible and has text");
    }

    @Step("Verify if Jewelry page Sort By Selection is visible")
    public void isJewelryPageSortBySelectionVisible() {
        sortBySelectOption.shouldBe(visible).shouldBe(enabled);
        takeScreenShot("Jewelry page Sort By Selection is visible");
    }

    @Step("Verify if Jewelry page Sort Switcher is visible")
    public void isJewelryPageSortSwitcherVisible() {
       sortSwitch.shouldBe(visible).shouldBe(enabled);
        takeScreenShot("Jewelry page Sort Switcher is visible");
    }

    @Step("Verify if Pearl Strand Necklace page is visible")
    public void isPearlStrandNecklacePageVisible() {
        inStock.shouldBe(visible).shouldBe(enabled).shouldHave(exactText("In stock"));
        pearlStrandNecklaceName.shouldBe(visible).shouldHave(exactText("Pearl Strand Necklace"));
        pearlStrandNecklacePrice.shouldBe(visible).shouldHave(exactText("250,00 LEU"));
        pearlStrandNecklaceShortDescription.shouldBe(visible).shouldHave(text("For a discreet display of pure " +
                "elegance. Layer multi strands or compliment with pearl or " +
                "diamond studs. 18"+'"'+ " or " + "24"+'"'));
        pearlStrandNecklaceImage.shouldBe(visible).shouldBe(enabled).hover();
        takeScreenShot("Pearl Strand Necklace page is visible");
    }

    @Step("Verify if Add To Cart button is visible")
    public void isAddToCartButtonVisible() {
        addToCartButton.shouldBe(visible).shouldBe(enabled).
                shouldHave(attribute("title","Add to Cart"));
        takeScreenShot("Add To Cart button is visible");
    }

    @Step("Verify if Choose an Option is visible, is required field and has text")
    public void isChooseAnOptionVisibleRequiredFieldAndHasText () {
        chooseAnOption.shouldBe(visible).shouldHave(cssClass("required-entry"));
        labelRequired.shouldBe(visible).shouldHave(text("Necklace Length"));
        takeScreenShot("Choose an Option is visible, is required field and has text");
    }

    @Step("Verify if required field is visible and has text")
    public void isRequiredFieldVisibleAndHasText () {
        validateRequiredFields.shouldBe(visible).shouldHave(exactText("This is a required field."));
    }

    @Step("Verify if Jewelry page link is visible and has text")
    public void isJewelryPageLinkVisibleAndHasText() {
        goToJewelryPage.shouldBe(visible).shouldHave(exactText("Jewelry"));
    }

    @Step("Verify if Accessories page link is visible and has text")
    public void isAccessoriesPageLinkVisibleAndHasText() {
        goToAccessoriesPage.shouldBe(visible).shouldHave(exactText("Accessories"));
    }

    @Step("Verify if Home page link is visible and has text")
    public void isHomePageLinkVisibleAndHasText() {
       goToHomePage.shouldBe(visible).shouldHave(attribute("title","Go to Home Page"));
    }

    @Step("Verify if Quantity product is visible and has title")
    public void isProdusctsQuantityVisibleAndHasTitle() {
        productsQuantity.shouldBe(visible).shouldHave(attribute("title","Qty"));
        takeScreenShot("Quantity product is visible and has title");
    }

    @Step("Verify if Quantity error message is visible and has title")
    public void isQuantityErrorMessageVisibleAndHasText() {
        quantityErrorMessage.shouldBe(visible)
                .shouldHave(text("The requested quantity for "
                      +'"'+"Pearl Strand Necklace"+'"'+" is not available."));
        takeScreenShot("Quantity error message is visible and has title");
    }

    @Step("Verify if Shirt Color and Size is visible and has title")
    public void isShirtColorAndSizeVisible() {
        shirtColor.shouldBe(visible).shouldBe(enabled);
        shirtSize.shouldBe(visible).shouldBe(enabled);
    }

    /**
     * Actions
     */

    @Step("Select all products from first level menu")
    public SelenideElement getSelectAllMenuProducts() {
      //  for (int i = 0; i < selectAllMenuProducts.size(); i++) {
        for (int i = 1; i < selectAllMenuProducts.size(); i++) {
            selectAllMenuProducts.get(i).shouldBe(visible).shouldBe(enabled).hover(); //.click();
        }
        takeScreenShot("Select all products from first level menu");
        return null;
    }

    @Step("Select specific products from first level menu")
    public SelenideElement getSelectSpecificMenuProducts(String identifier) {
       // for (int i = 0; i < selectAllMenuProducts.size(); i++) {
        for (int i = 1; i < selectAllMenuProducts.size(); i++) {
            if (selectAllMenuProducts.get(i).getText().equals(identifier)) {
                selectAllMenuProducts.get(i).shouldBe(visible).shouldBe(enabled).hover().click();
                return selectAllMenuProducts.get(i);
            }
        }
        takeScreenShot("Select specific products from first level menu");
       return null;
    }

    @Step("Select all submenu products")
    public SelenideElement getSelectAllSubmenuProducts() {
        //int k = 1; Meniul CD-uri nu se mai parcurge;
        int k = 2;
        do {
            ElementsCollection selectParentElement = $$(".level0" + ".nav-" + k + " a.level0");
            ElementsCollection selectChildElement = $$(".level0" + ".nav-" + k + " a.level1");
            System.out.println(selectParentElement);
            System.out.println(selectChildElement);

            for (int i = 0; i < selectParentElement.size(); i++) {
                selectParentElement.get(i).shouldBe(visible).shouldBe(enabled).hover();
                for (int j = 0; j < selectChildElement.size(); j++) {
                    selectChildElement.get(j).shouldBe(visible).shouldBe(enabled).hover();
                    ElementsCollection selectSubmenuForChildElement =
                            $$(".level1" + ".nav-" + k + "-" + j + " a.level2");
                    System.out.println(selectSubmenuForChildElement);
                    for (int m = 0; m < selectSubmenuForChildElement.size(); m++) {
                        selectSubmenuForChildElement.get(m).shouldBe(visible).shouldBe(enabled).hover();
                        sleep(1000);
                    }
                }
            }
            k++;
        } while (k <= selectAllMenuProducts.size());
        takeScreenShot("Select all submenu products");
        return null;
    }

    public SelenideElement getChildElement() {
        int nr = selectAllMenuProducts.size();
        for (int i = 0; i < selectAllMenuProducts.size(); i++) {
            if (i > 0 && i < nr) {
                String elememtUl = ".level0" + ".nav-" + i + "ul.level0";
                System.out.println(elememtUl);
            }
        }
        return null;
    }

    @Step("Select specific submenu products from second level")
    public SelenideElement getSelectSpecificSubmenuProductsLevelTwo(String identifier) {
      //int k = 1; Meniul CD-uri nu se mai parcurge;
      int k = 2;
        do {
            ElementsCollection selectParentElement = $$(".level0" + ".nav-" + k + " a.level0");
            ElementsCollection selectChildElement = $$(".level0" + ".nav-" + k + " a.level1");
            System.out.println(selectParentElement);
            System.out.println(selectChildElement);

            for (int i = 0; i < selectParentElement.size(); i++) {
                selectParentElement.get(i).shouldBe(visible).shouldBe(enabled).hover();
                for (int j = 0; j < selectChildElement.size(); j++) {
                    selectChildElement.get(j).shouldBe(visible).shouldBe(enabled).hover();
                    if (selectChildElement.get(j).getText().equals(identifier)) {
                        selectChildElement.get(j).shouldBe(visible).shouldBe(enabled).hover().click();
                        return selectChildElement.get(j);
                    }
                }
            }
            k++;
        } while (k <= selectAllMenuProducts.size());
        takeScreenShot("Select specific submenu products from second level");
        return null;
    }

    @Step("Select specific submenu products from third level")
    public SelenideElement getSelectSpecificSubmenuProductsLevelThree(String identifier) {
        //int k = 1; Meniul CD-uri nu se mai parcurge;
        int k = 2;
        do {
            ElementsCollection selectParentElement = $$(".level0" + ".nav-" + k + " a.level0");
            ElementsCollection selectChildElement = $$(".level0" + ".nav-" + k + " a.level1");
            System.out.println(selectParentElement);
            System.out.println(selectChildElement);

            for (int i = 0; i < selectParentElement.size(); i++) {
                selectParentElement.get(i).shouldBe(visible).shouldBe(enabled).hover();
                for (int j = 0; j < selectChildElement.size(); j++) {
                    selectChildElement.get(j).shouldBe(visible).shouldBe(enabled).hover();
                    ElementsCollection selectSubmenuForChildElement =
                            $$(".level1" + ".nav-" + k + "-" + j + " a.level2");
                    System.out.println(selectSubmenuForChildElement);
                    for (int m = 0; m < selectSubmenuForChildElement.size(); m++) {
                        selectSubmenuForChildElement.get(m).shouldBe(visible).shouldBe(enabled).hover();
                        if (selectSubmenuForChildElement.get(m).getText().equals(identifier)) {
                            selectSubmenuForChildElement.get(m).shouldBe(visible).shouldBe(enabled).hover().click();
                            return selectSubmenuForChildElement.get(m);
                        }
                    }
                }
            }
            k++;
        } while (k <= selectAllMenuProducts.size());
        takeScreenShot("Select specific submenu products from third level");
        return null;
    }

    @Step("Select specific products from the specific submenu products")
    public SelenideElement getSelectSpecificProductFromSpecificSubMenuProducts(String identifier) {
        for (int i = 0; i < productSubMenuList.size(); i++) {
            if (productSubMenuList.get(i).getAttribute("title").equals(identifier)) {
                productSubMenuList.get(i).shouldBe(visible).shouldBe(enabled).hover().click();
                return productSubMenuList.get(i);
            }
        }
        takeScreenShot("Select specific products from the specific submenu products");
        return null;
    }

    @Step("Click on the Dress Red product")
    public void clickOnDressRedProduct() {
        dressRedProduct.hover().click();
    }

    @Step("Click on the Dress Sakura product")
    public void clickOnDressSakuraProduct() {
        dressRedProduct.hover().click();
    }

    @Step("Zoom the image products")
    public void setZoomImage() {
       zoomImage.shouldBe(visible).hover();
    }

    @Step("Zoom the windows image")
    public void setZoomWindowImage() {
        zoomWindowImage.shouldBe(visible).hover();
    }

    @Step("Click on Pearl Strand product")
    public void clickOnPearlStrandProduct() {
        pearlStrandProduct.hover().click();
    }

    @Step("Click on Pearl Strand product View Details")
    public void clickOnPearlStrandProducViewDetails() {
        pearlStrandProductViewDetails.hover().click();
    }

    @Step("Click on Jewelry page Sort ByS election")
    public void clickOnJewelryPageSortBySelection() {
        sortBySelectOption.hover().click();
    }

    @Step("Sort Jewelry page By Position Ascending")
    public void sortJewelryPageByPositionAscending() {
        sortBySelectOption.selectOption("Position");
        if (sortSwitch.getText().equals("Set Ascending Direction")) {
            sortSwitch.hover().click();
        }
        takeScreenShot("Sort Jewelry page By Position Ascending");
    }

    @Step("Sort Jewelry page By Position Descending")
    public void sortJewelryPageByPositionDescending() {
        sortBySelectOption.selectOption("Position");
        if (sortSwitch.getText().equals("Set Descending Direction")) {
            sortSwitch.hover().click();
        }
        takeScreenShot("Sort Jewelry page By Position Descending");
    }

    @Step("Sort Jewelry page By Name Ascending")
    public void sortJewelryPageByNameAscending() {
        sortBySelectOption.selectOption("Name");
        if (sortSwitch.getText().equals("Set Ascending Direction")) {
            sortSwitch.hover().click();
        }
        takeScreenShot("Sort Jewelry page By Name Ascending");
    }

    @Step("Sort Jewelry page By Name Descending")
    public void sortJewelryPageByNameDescending() {
        sortBySelectOption.selectOption("Name");
        if (sortSwitch.getText().equals("Set Descending Direction")) {
            sortSwitch.hover().click();
        }
        takeScreenShot("Sort Jewelry page By Name Descending");
    }

    @Step("Sort Jewelry page By Price Ascending")
    public void sortJewelryPageByPriceAscending() {
        sortBySelectOption.selectOption("Price");
        if (sortSwitch.getText().equals("Set Ascending Direction")) {
            sortSwitch.hover().click();
        }
        takeScreenShot("Sort Jewelry page By Price Ascending");
    }

    @Step("Sort Jewelry page By Price Descending")
    public void sortJewelryPageByPriceDescending() {
        sortBySelectOption.selectOption("Price");
        if (sortSwitch.getText().equals("Set Descending Direction")) {
            sortSwitch.hover().click();
        }
        takeScreenShot("Sort Jewelry page By Price Descending");
    }

    @Step("Click on Pearl Strand Necklace tabs")
    public SelenideElement clickOnPearlStrandNecklaceTabs() {
        for (int i = 0; i < pearlStrandNecklaceTabs.size(); i++) {
            pearlStrandNecklaceTabs.get(i).scrollTo().shouldBe(visible).hover().click();
        }
        takeScreenShot("Click on Pearl Strand Necklace tabs");
        return null;
    }

    @Step("Click on Add To Cart button")
    public void clickOnAddToCartButton() {
        addToCartButton.hover().click();
    }

    @Step("Click on Jewelry page link")
    public void clickOnJewelryPageLink() {
        goToJewelryPage.hover().click();
    }

    @Step("Click on Accessories page link")
    public void clickOnAccessoriesPageLink() {
        goToAccessoriesPage.hover().click();
    }

    @Step("Click on Home page link")
    public void clickOnHomePageLink() {
        goToHomePage.hover().click();
    }

    @Step("Click on Choose an Option popup menu")
    public void clickOnChooseAnOption() {
        chooseAnOption.hover().click();
        chooseAnOption.scrollTo().selectOption(1);
        takeScreenShot("Click on Choose an Option popup menu");
    }

    @Step("Select submenu Jewelry products")
    public void selectSubmenuJewelryProducts() {
        menuAccessories.shouldBe(visible).shouldBe(enabled).hover();
        subMenuJewelry.shouldBe(visible).shouldBe(enabled).hover().click();
    }

    @Step("Fill in Quantity product field")
    public void fillInProductsQuantity(String input){
       productsQuantity.clear();
       productsQuantity.sendKeys(input);
       takeScreenShot("Fill in Quantity product field");
    }

    @Step("Click nn Shirt Color")
    public void clickOnShirtColor() {
        shirtColor.hover().click();
    }

    @Step("Click nn Shirt Size")
    public void clickOnShirtSize() {
        shirtSize.hover().click();
    }

}
