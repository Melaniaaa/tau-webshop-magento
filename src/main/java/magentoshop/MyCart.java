package magentoshop;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.Integer.valueOf;
import static org.apache.commons.lang3.StringUtils.indexOf;
import static org.apache.commons.lang3.StringUtils.startsWith;

public class MyCart extends ScreenShooter {

    private final SelenideElement cartTitle = $(".cart .page-title h1");
    private final SelenideElement successCartMessage = $(".cart .success-msg span");
    private final SelenideElement deleteCartButton = $(".product-cart-remove a.btn-remove.btn-remove2");
    private final SelenideElement closeMiniHeaderView = $("a.close.skip-link-close");
    private final SelenideElement removeItemsFromMiniHeaderCart = $(".mini-products-list a.remove");
    private final SelenideElement emptyCartButton = $("#empty_cart_button");
    private final SelenideElement continueShoppingCartButton = $(".button2.btn-continue");
    private final ElementsCollection updateShoppingCartButton = $$(".button2.btn-update");
    private final SelenideElement checkoutCartButton = $(".bottom button.btn-proceed-checkout.btn-checkout");
    private final SelenideElement editCartButton = $(".product-cart-actions .cart-links a[title*='Edit']");
    private final SelenideElement productCartPrice = $(".product-cart-price .cart-price span.price");
    private final SelenideElement productCartQuantity = $(".product-cart-actions .input-text.qty");
    private final SelenideElement productCartTotal = $(".product-cart-total .cart-price span.price");
    private final ElementsCollection freeFlatShippingRadioButton = $$(".sp-methods .radio");
    private final SelenideElement freeShippingRadioButton =
            $("#co-shipping-method-form #s_method_freeshipping_freeshipping.radio");
    private final SelenideElement flatShippingRadioButton =
            $("#co-shipping-method-form #s_method_flatrate_flatrate.radio");
    private final SelenideElement updateTotalButton = $(".buttons-set .button");

    /**
     * Validators
     */

    @Step("Verify Cart Page is visible and has text")
    public void isCartPageVisibleAndHasText() {
        cartTitle.shouldBe(visible).shouldHave(exactText("Shopping Cart"));
        takeScreenShot("Cart Page is visible and has text");
    }

    @Step("Verify success Cart Message is visible and has text")
    public void isSuccessMessageVisibleAndHasText() {
        successCartMessage.shouldBe(visible).shouldHave(exactText("Pearl Strand Necklace was added to your shopping cart."));
        takeScreenShot("Cart Page is visible and has text");
    }

    @Step("Verify if Delete Cart button is visible")
    public void isDeleteCartButtonVisible() {
        deleteCartButton.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify if Edit Cart link is visible and has text")
    public void isEditCartButtonVisibleAndHasText() {
        editCartButton.shouldBe(visible).shouldBe(enabled);
        editCartButton.shouldHave(exactText("Edit"));
    }

    @Step("Verify if Close button from Mini Header Cart is visible")
    public void isCloseMiniHeaderCartVisibleAndHasTitle() {
        closeMiniHeaderView.shouldBe(visible).shouldBe(enabled);
        closeMiniHeaderView.shouldHave(attribute("title", "Close"));
        takeScreenShot("Cart Page is visible and has text");
    }

    @Step("Verify if Remove Items from Mini Header Cart is visible")
    public void isRemoveItemsFromMiniHeaderCartVisibleAndHasTitle() {
        removeItemsFromMiniHeaderCart.shouldBe(visible).shouldBe(enabled);
        removeItemsFromMiniHeaderCart.shouldHave(exactText("Remove Item"));
        takeScreenShot("Cart Page is visible and has text");
    }

    @Step("Verify if Empty Cart button is visible and has title")
    public void isEmptyCartButtonVisibleAndHasTitle() {
        emptyCartButton.shouldBe(visible).shouldBe(enabled);
        emptyCartButton.shouldHave(attribute("title", "Empty Cart"));
        takeScreenShot("Cart Page is visible and has text");
    }

    @Step("Verify if Continue Shopping Cart button is visible and has title")
    public void isContinueShoppingCartButtonVisibleAndHasTitle() {
        continueShoppingCartButton.shouldBe(visible).shouldBe(enabled);
        continueShoppingCartButton.shouldHave(attribute("title", "Continue Shopping"));
    }

    @Step("Verify if Update Shopping Cart button is visible and has title")
    public void isUpdateShoppingCartButtonVisibleAndHasTitle() {
        updateShoppingCartButton.get(1).shouldBe(visible).shouldBe(enabled);
        updateShoppingCartButton.get(1).shouldHave(attribute("title", "Update Shopping Cart"));
    }

    @Step("Verify if Checkout Cart button is visible and has title")
    public void isCheckoutCartButtonVisibleAndHasTitle() {
        checkoutCartButton.shouldBe(visible).shouldBe(enabled);
        checkoutCartButton.shouldHave(attribute("title", "Proceed to Checkout"));
    }

    @Step("Verify if MyCart Update Total button is visible and has title")
    public void isUpdateTotalButtonVisibleAndHasTitle() {
        updateTotalButton.shouldBe(visible).shouldBe(enabled);
        updateTotalButton.shouldHave(attribute("title", "Update Total"));
    }

    /**
     * Actions
     */

    @Step("Click on Delete Cart button")
    public void clickOnDeleteCartButton() {
        deleteCartButton.hover().click();
    }

    @Step("Click on Edit Cart link")
    public void clickOnEditCartButton() {
        editCartButton.hover().click();
    }

    @Step("Click on Close Mini Header Cart view")
    public void clickOnCloseMiniHeaderView() {
        closeMiniHeaderView.hover().click();
    }

    @Step("Click on Remove Items from Mini Header Cart")
    public void clickOnRemoveItemsFromMiniHeaderCart() {
        removeItemsFromMiniHeaderCart.hover().click();
        Selenide.switchTo().alert().accept();
        takeScreenShot("Remove Items from Mini Header Cart");
    }

    @Step("Click on Empty Cart button")
    public void clickOnEmptyCartButton() {
        emptyCartButton.shouldBe(visible).shouldBe(enabled).hover().click();
    }

    @Step("Click on Continue Shopping Cart button")
    public void clickOnContinueShoppingCartButton() {
        continueShoppingCartButton.shouldBe(visible).shouldBe(enabled).hover().click();
    }

    @Step("Click on Update Shopping Cart button")
    public void clickOnUpdateShoppingCartButton() {
        updateShoppingCartButton.get(1).hover().click();
    }

    @Step("Click on Checkout Cart button")
    public void clickOnCheckoutCartButton() {
        checkoutCartButton.shouldBe(visible).shouldBe(enabled).hover().click();
        takeScreenShot("Click on Checkout Cart button");
    }

    @Step("Click on Free Shipping radio button from Cart page")
    public void clickOnFreeShippingRadioOption() {
       // freeFlatShippingRadioButton.get(0).scrollTo().hover().click();
        freeShippingRadioButton.scrollTo().hover().click();
    }

    @Step("Click on Flate Rate radio button from Cart page")
    public void clickOnFlateRateRadioOption() {
        //freeFlatShippingRadioButton.get(1).scrollTo().hover().click();
        flatShippingRadioButton.scrollTo().hover().click();
    }

    @Step("Check on Subtotal Price from Cart page")
    public void checkSubtotalPriceCartPage() {
        productCartPrice.shouldBe(visible);
        productCartQuantity.shouldBe(visible);
        productCartTotal.shouldBe(visible);
        String price = productCartPrice.getText().substring(0,productCartPrice.getText().indexOf(","));
        String qty = productCartQuantity.getValue();
        String total = productCartTotal.getText().substring(0,productCartTotal.getText().indexOf(","));
        System.out.println(valueOf(price));
        System.out.println(qty);
        if (total.length()>3){
            total = total.substring(0,total.indexOf(".")).concat(total.substring(2));
            System.out.println(total);
        }
        if (valueOf(total).equals(valueOf(price)*valueOf(qty))){
            System.out.println("Subtotal Shopping Cart corect calculat!");
            System.out.println(total + " = " + price + "*" + qty);
        }
        takeScreenShot("Check on Subtotal Price from Cart page");
    }

    @Step("Click on Update Total button from Cart page")
    public void clickOnUpdateTotalButton() {
        updateTotalButton.scrollTo().hover().click();
        takeScreenShot("Click on Update Total button from Cart page");
    }
}


