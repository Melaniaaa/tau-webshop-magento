package magentoshop;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Checkout extends ScreenShooter {

    private final SelenideElement checkoutContinueButton = $("#billing-buttons-container button");
    private final ElementsCollection checkoutShipAddress = $$(".control .radio");
    private final ElementsCollection checkoutFreeFlatShipping = $$(".sp-methods .radio");
    private final SelenideElement checkoutFreeShipping =
            $("#checkout-shipping-method-load #s_method_freeshipping_freeshipping.radio");
    private final SelenideElement checkoutFlatShipping =
            $("#s_method_flatrate_flatrate.radio.validation-passed");
    // $("#checkout-shipping-method-load #s_method_flatrate_flatrate.radio.validation-passed");
    private final SelenideElement checkoutShippingMethodContinueButton =
            $("#shipping-method-buttons-container .button");
    private final SelenideElement checkoutShippingMethodBackLink =
         //   $("#shipping-method-buttons-container .back-link");
            $("#shipping-method-buttons-container .back-link a[href='#']");
    private final SelenideElement checkoutPaymentInformationContinueButton =
            $("#payment-buttons-container button");
    private final SelenideElement checkoutPaymentInformationBackLink =
            $("#payment-buttons-container .back-link a");
    private final SelenideElement checkoutPlaceOrderButton = $("#review-buttons-container button");
    private final SelenideElement checkoutContinueShoppingButton = $(".buttons-set button");

    /**
     * Verifiers
     */

    @Step("Verify Checkout button is visible and has title")
    public void isCheckoutContinueButtonVisibleAndHasTitle() {
       checkoutContinueButton.shouldBe(visible).shouldBe(enabled);
       checkoutContinueButton.shouldHave(attribute("title", "Continue"));
        takeScreenShot();
    }

    @Step("Verify Checkout Ship Address is visible and has title")
    public void isCheckoutShipAddressVisibleAndHasTitle() {
        checkoutShipAddress.get(0).shouldBe(visible).shouldBe(enabled);
        checkoutShipAddress.get(0).shouldHave(attribute("title", "Ship to this address"));
        takeScreenShot();
    }

    @Step("Verify Checkout Free Shipping is visible")
    public void isCheckoutFreeShippingVisible() {
       // checkoutFreeFlatShipping.get(0).shouldBe(visible).shouldBe(enabled);
        checkoutFreeShipping.shouldBe(visible).shouldBe(enabled);
        takeScreenShot();
    }

    @Step("Verify Checkout Flat Shipping is visible")
    public void isCheckoutFlatShippingVisible() {
       // checkoutFreeFlatShipping.get(1).shouldBe(visible).shouldBe(enabled);
        checkoutFlatShipping.shouldBe(visible).shouldBe(enabled);
        takeScreenShot();
    }

    @Step("Verify Checkout Shipping Method Continue button is visible")
    public void isCheckoutShippingMethodContinueButtonVisible() {
        checkoutShippingMethodContinueButton.shouldBe(visible).shouldBe(enabled);
        takeScreenShot();
    }

    @Step("Verify Checkout Shipping Method Back link is visible")
    public void isCheckoutShippingMethodBackLinkVisible() {
        checkoutShippingMethodBackLink.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify Checkout Payment Information Continue button is visible")
    public void isCheckoutPaymentInformationContinueButtonVisible() {
        checkoutPaymentInformationContinueButton.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify Checkout Payment Information Back link is visible")
    public void isCheckoutPaymentInformationBackLinkVisible() {
        checkoutPaymentInformationBackLink.shouldBe(visible).shouldBe(enabled);
    }

    @Step("Verify Checkout Plase Order button is visible")
    public void isCheckoutPlaseOrderButtonVisibleAndHasTitle() {
        checkoutPlaceOrderButton.shouldBe(visible).shouldBe(enabled);
        checkoutPlaceOrderButton.shouldHave(attribute("title","Place Order"));
        takeScreenShot();
    }

    @Step("Verify Checkout Continue Shopping is visible")
    public void isCheckoutContinueShoppingButtonVisible() {
        checkoutContinueShoppingButton.shouldBe(visible).shouldBe(enabled);
        takeScreenShot();
    }

    /**
     * Actions
     */

    @Step("Click on Checkout Continue button")
    public void clickOnCheckoutContinueButton() {
        checkoutContinueButton.scrollTo().hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Ship Address radio button")
    public void clickOnCheckoutShipAddressRadioButton() {
        checkoutShipAddress.get(0).hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Free Shipping radio button")
    public void clickOnCheckoutFreeShippingRadioButton() {
      //  checkoutFreeFlatShipping.get(0).hover().click();
        checkoutFreeShipping.hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Flat Shipping radio button")
    public void clickOnCheckoutFlatShippingRadioButton() {
       // checkoutFreeFlatShipping.get(1).scrollTo().hover().click();
        checkoutFlatShipping.scrollTo().hover().scrollTo();
        takeScreenShot();
    }

    @Step("Click on Checkout Shipping Method Continue button")
    public void clickOnCheckoutShippingMethodContinueButton() {
        checkoutShippingMethodContinueButton.scrollTo().hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Shipping Method Back link")
    public void clickOnCheckoutShippingMethodBackLink() {
        checkoutShippingMethodBackLink.hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Payment Information Continue button")
    public void clickOnCheckoutPaymentInformationContinueButton() {
        checkoutPaymentInformationContinueButton.hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Payment Information Back link")
    public void clickOnCheckoutPaymentInformationBackLink() {
        checkoutPaymentInformationBackLink.hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Plase Order button")
    public void clickOnCheckoutPlaseOrderButton() {
        checkoutPlaceOrderButton.hover().click();
        takeScreenShot();
    }

    @Step("Click on Checkout Continue Shopping button")
    public void clickOnCheckoutContinueShoppingButton() {
        checkoutContinueShoppingButton.hover().click();
        takeScreenShot();
    }
}
