package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("RegisterTests")
public class RegisterTests {
    MagentoShopBasePage magentoShopBasePage;
    Account account;
    Register register;

    @BeforeClass
    public void setup() {
        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("Register")
    @Test
    public void verify_if_first_name_is_required_field_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isFirstNameVisible();
        register.isFirstNameRequiredFieldAndHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_middle_name_is_not_required_field_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isMiddleNameVisible();
        register.isMiddleNameNotRequiredFieldAndHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_last_name_is_required_field_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isLastNameVisible();
        register.isLastNameRequiredFieldAndHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_email_address_is_required_field_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isRegisterEmailAddressInputVisible();
        register.isRegisterEmailAddressInputRequiredFieldAndHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_password_is_required_field_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isRegisterPasswordInputVisible();
        register.isRegisterPasswordInputRequiredFieldAndHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_confirm_password_is_required_field_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isRegisterConfirmPasswordInputVisible();
        register.isRegisterConfirmPasswordInputRequiredFieldAndHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_register_button_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isRegisterButtonVisible();
        register.isRegisterButtonHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_if_subscribed_is_checkbox_and_has_title() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isSubscribedInputVisible();
        register.isSubscribedInputCheckbox();
        register.isSubscribedInputHasTitle();
    }

    @Feature("Register")
    @Test
    public void verify_select_subscribed_checkbox() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isSubscribedInputVisible();
        register.selectSubscribedCheckBox();
    }

    @Feature("Register")
    @Test
    public void can_register_with_valid_credentials() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isFirstNameVisible();
        register.fillInFirstName("TUSA");
        register.isLastNameVisible();
        register.fillInLastName("LETITIA-MELANIA");
        register.isRegisterEmailAddressInputVisible();
        register.fillInRegisterEmailAddress("tlm@gmail.com");
        register.isRegisterPasswordInputVisible();
        register.fillInRegisterPassword("leti03");
        register.isRegisterConfirmPasswordInputVisible();
        register.fillInRegisterConfirmPassword("leti03");
        register.isRegisterButtonVisible();
        register.clickOnRegisterButton();
        register.isSuccessMessageVisibleAndHasText();
        magentoShopBasePage.clickAccountList();
        //   account.clickLogOut();
    }

    @Feature("Register")
    @Test
    public void verify_register_with_existing_credentials() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isFirstNameVisible();
        register.fillInFirstName("TUSA");
        register.isLastNameVisible();
        register.fillInLastName("LETITIA-MELANIA");
        register.isRegisterEmailAddressInputVisible();
        register.fillInRegisterEmailAddress("tlm@gmail.com");
        register.isRegisterPasswordInputVisible();
        register.fillInRegisterPassword("leti03");
        register.isRegisterConfirmPasswordInputVisible();
        register.fillInRegisterConfirmPassword("leti03");
        register.selectSubscribedCheckBox();
        register.isRegisterButtonVisible();
        register.clickOnRegisterButton();
        register.isErrorMessageVisibleAndHasText();
    }

    @Feature("Register")
    @Test
    public void verify_register_with_blank_all_credentials() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isRegisterButtonVisible();
        register.clickOnRegisterButton();
        register.isValidationMessageFirstNameBlankVisibleAndHasText();
        register.isValidationMessageLastNameBlankVisibleAndHasText();
        register.isValidationMessageEmailAddressBlankVisibleAndHasText();
        register.isValidationMessagePasswordBlankVisibleAndHasText();
        register.isValidationMessageConfirmPasswordBlankVisibleAndHasText();
    }

    @Feature("Register")
    @Test
    public void verify_register_with_first_name_blank() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isFirstNameVisible();
        register.fillInFirstName("");
        register.isLastNameVisible();
        register.fillInLastName("LETITIA-MELANIA");
        register.isRegisterEmailAddressInputVisible();
        register.fillInRegisterEmailAddress("tlm@gmail.com");
        register.isRegisterPasswordInputVisible();
        register.fillInRegisterPassword("leti03");
        register.isRegisterConfirmPasswordInputVisible();
        register.fillInRegisterConfirmPassword("leti03");
        register.isRegisterButtonVisible();
        register.clickOnRegisterButton();
        register.isValidationMessageFirstNameBlankVisibleAndHasText();
    }

    @Feature("Register")
    @Test
    public void verify_register_with_wrong_confirm_password() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isFirstNameVisible();
        register.fillInFirstName("Popescu");
        register.isLastNameVisible();
        register.fillInLastName("Ileana");
        register.isRegisterEmailAddressInputVisible();
        register.fillInRegisterEmailAddress("tlm@gyahoo.com");
        register.isRegisterPasswordInputVisible();
        register.fillInRegisterPassword("leti04");
        register.isRegisterConfirmPasswordInputVisible();
        register.fillInRegisterConfirmPassword("leti08");
        register.isRegisterButtonVisible();
        register.clickOnRegisterButton();
        register.isValidationMessageConfirmPasswordVisibleAndHasText();
    }

    @Feature("Register")
    @Test
    public void verify_if_you_can_back_from_register_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
        register = new Register();
        register.isRegisterBackLinkVisible();
        register.clickOnRegisterBackLink();
    }
}