package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("CheckoutTests")
public class CheckoutTests {
    MagentoShopBasePage magentoShopBasePage;
    Account account;
    Login login;
    Products products;
    CartIsEmpty cartIsEmpty;
    MyCart myCart;
    Checkout checkout;

    @BeforeClass
    public void setup() {
        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("Checkout")
    @Test
    public void verify_checkout_empty_page_title_message() {
        String pageTitle = "Shopping Cart is Empty";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openCheckout();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifyPageTitleWhenCartIsEmpty(pageTitle);
    }

    @Feature("Checkout")
    @Test
    public void verify_checkout_empty_first_message() {
        String firstMessage = "You have no items in your shopping cart.";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openCheckout();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifyFirstMessageWhenCartIsEmpty(firstMessage);
    }

    @Feature("Checkout")
    @Test
    public void verify_checkout_empty_second_message() {
        String secondMessage = "here";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openCheckout();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifySecondMessageWhenCartIsEmpty(secondMessage);
    }

    @Feature("Checkout")
    @Test
    public void verify_continue_shopping_when_checkout_is_empty() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openCheckout();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.continueShoppingWhenCartIsEmpty();
    }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout() {
            magentoShopBasePage.verifySelectLanguageTwo("English");
            magentoShopBasePage.openAccountList();
            account = new Account();
            account.openLogin();
            login = new Login();
            login.fillInEmailAddress("TLM@gmail.com");
            login.fillInPassword("leti03");
            login.clickOnLoginButton();
            products = new Products();
         //   products.getSelectSpecificSubmenuProductsLevelTwo("Jewelry");
         //   products.getSelectSpecificProductFromSpecificSubMenuProducts("Pearl Strand Necklace");
         //   products.clickOnChooseAnOption();
            products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
            products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
            products.fillInProductsQuantity("1");
            products.clickOnAddToCartButton();
            myCart = new MyCart();
            myCart.clickOnUpdateShoppingCartButton();
            myCart.clickOnFreeShippingRadioOption();
            myCart.isCheckoutCartButtonVisibleAndHasTitle();
            myCart.clickOnCheckoutCartButton();
            checkout = new Checkout();
            checkout.isCheckoutShipAddressVisibleAndHasTitle();
            checkout.clickOnCheckoutShipAddressRadioButton();
            checkout.isCheckoutContinueButtonVisibleAndHasTitle();
            checkout.clickOnCheckoutContinueButton();
            magentoShopBasePage.openAccountList();
            account.clickLogOut();
        }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout_free_shipping_from_shipping_method() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.clickOnCheckoutCartButton();
        checkout = new Checkout();
        checkout.clickOnCheckoutShipAddressRadioButton();
        checkout.clickOnCheckoutContinueButton();
        checkout.isCheckoutFreeShippingVisible();
        checkout.clickOnCheckoutFreeShippingRadioButton();
        checkout.isCheckoutShippingMethodContinueButtonVisible();
        checkout.clickOnCheckoutShippingMethodContinueButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout_flat_shipping_from_shipping_method() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.isCheckoutCartButtonVisibleAndHasTitle();
        myCart.clickOnCheckoutCartButton();
        checkout = new Checkout();
        checkout.clickOnCheckoutShipAddressRadioButton();
        checkout.clickOnCheckoutContinueButton();
    //    checkout.isCheckoutFlatShippingVisible();
    //    checkout.clickOnCheckoutFlatShippingRadioButton();
        checkout.isCheckoutShippingMethodContinueButtonVisible();
        checkout.clickOnCheckoutShippingMethodContinueButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout_back_link_from_shipping_method_page() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.clickOnCheckoutCartButton();
        checkout = new Checkout();
        checkout.clickOnCheckoutShipAddressRadioButton();
        checkout.clickOnCheckoutContinueButton();
      //  checkout.isCheckoutFlatShippingVisible();
      //  checkout.clickOnCheckoutFlatShippingRadioButton();
      //  checkout.isCheckoutShippingMethodBackLinkVisible();
      //  checkout.clickOnCheckoutShippingMethodBackLink();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout_continue_button_from_payment_information_page() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.clickOnCheckoutCartButton();
        checkout = new Checkout();
        checkout.clickOnCheckoutShipAddressRadioButton();
        checkout.clickOnCheckoutContinueButton();
      //  checkout.clickOnCheckoutFlatShippingRadioButton();
        checkout.clickOnCheckoutShippingMethodContinueButton();
        checkout.isCheckoutPaymentInformationContinueButtonVisible();
        checkout.clickOnCheckoutPaymentInformationContinueButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout_back_link_from_payment_information_page() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.clickOnCheckoutCartButton();
        checkout = new Checkout();
        checkout.clickOnCheckoutShipAddressRadioButton();
        checkout.clickOnCheckoutContinueButton();
     //   checkout.clickOnCheckoutFlatShippingRadioButton();
        checkout.clickOnCheckoutShippingMethodContinueButton();
     //   checkout.isCheckoutPaymentInformationBackLinkVisible();
     //   checkout.clickOnCheckoutPaymentInformationBackLink();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Checkout")
    @Test
    public void can_proceed_to_checkout_place_order_from_order_review_page() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.clickOnCheckoutCartButton();
        checkout = new Checkout();
        checkout.clickOnCheckoutShipAddressRadioButton();
        checkout.clickOnCheckoutContinueButton();
       // checkout.clickOnCheckoutFlatShippingRadioButton();
        checkout.clickOnCheckoutShippingMethodContinueButton();
        checkout.clickOnCheckoutPaymentInformationContinueButton();
        checkout.isCheckoutPlaseOrderButtonVisibleAndHasTitle();
        checkout.clickOnCheckoutPlaseOrderButton();
        checkout.isCheckoutContinueShoppingButtonVisible();
        checkout.clickOnCheckoutContinueShoppingButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }
}

