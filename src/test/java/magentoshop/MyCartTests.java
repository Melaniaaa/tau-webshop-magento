package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("MyCartTests")
public class MyCartTests {
    MagentoShopBasePage magentoShopBasePage;
    Account account;
    Login login;
    Products products;
    CartIsEmpty cartIsEmpty;
    MyCart myCart;

    @BeforeClass
    public void setup() {
        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("MyCart")
    @Test
    public void verify_mycart_empty_page_title_message() {
        String pageTitle = "Shopping Cart is Empty";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyCart();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifyPageTitleWhenCartIsEmpty(pageTitle);
    }

    @Feature("MyCart")
    @Test
    public void verify_mycart_empty_first_message() {
        String firstMessage = "You have no items in your shopping cart.";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyCart();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifyFirstMessageWhenCartIsEmpty(firstMessage);
    }

    @Feature("MyCart")
    @Test
    public void verify_mycart_empty_second_message() {
        String secondMessage = "here";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyCart();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifySecondMessageWhenCartIsEmpty(secondMessage);
    }

    @Feature("MyCart")
    @Test
    public void verify_continue_shopping_when_mycart_is_empty() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyCart();
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.continueShoppingWhenCartIsEmpty();
    }

    @Feature("MyCart")
    @Test
    public void can_add_products_in_mycart_page() {
        String identifier = "Jewelry";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.clickOnPearlStrandProduct();
        products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        magentoShopBasePage.clickOnMiniHeaderCart();
        myCart.isRemoveItemsFromMiniHeaderCartVisibleAndHasTitle();
        myCart.isCloseMiniHeaderCartVisibleAndHasTitle();
        myCart.clickOnRemoveItemsFromMiniHeaderCart();
        myCart.clickOnCloseMiniHeaderView();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void verify_title_when_mycart_page_is_not_empty() {
        String identifier = "Jewelry";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.clickOnPearlStrandProduct();
        products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.isCartPageVisibleAndHasText();
        myCart.isSuccessMessageVisibleAndHasText();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void can_delete_products_from_mycart_page() {
        String identifier = "Jewelry";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.clickOnPearlStrandProduct();
        products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.isDeleteCartButtonVisible();
        myCart.clickOnDeleteCartButton();
        String pageTitle = "Shopping Cart is Empty";
        String firstMessage = "You have no items in your shopping cart.";
        String secondMessage = "here";
        cartIsEmpty = new CartIsEmpty();
        cartIsEmpty.verifyPageTitleWhenCartIsEmpty(pageTitle);
        cartIsEmpty.verifyFirstMessageWhenCartIsEmpty(firstMessage);
        cartIsEmpty.verifySecondMessageWhenCartIsEmpty(secondMessage);
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

   @Feature("MyCart")
   @Test
    public void verify_if_mini_header_cart_is_empty() {
      magentoShopBasePage.verifySelectLanguageTwo("English");
      magentoShopBasePage.openAccountList();
      account = new Account();
      account.openLogin();
      login = new Login();
      login.fillInEmailAddress("TLM@gmail.com");
      login.fillInPassword("leti03");
      login.clickOnLoginButton();
      myCart = new MyCart();
      while (magentoShopBasePage.verifyMiniHeaderCartIsEmpty()) {
          magentoShopBasePage.clickOnMiniHeaderCart();
          myCart.clickOnRemoveItemsFromMiniHeaderCart();
          myCart.clickOnCloseMiniHeaderView();
      }
      magentoShopBasePage.openAccountList();
      account.clickLogOut();
  }

    @Feature("MyCart")
    @Test
    public void can_empty_mycart_page() {
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
    //   products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.isEmptyCartButtonVisibleAndHasTitle();
        myCart.clickOnEmptyCartButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void can_update_shopping_cart_page() {
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        //   products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.isEditCartButtonVisibleAndHasText();
        myCart.clickOnEditCartButton();
        products.fillInProductsQuantity("2");
        products.clickOnAddToCartButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void verify_calculation_subtotal_from_mycart_page() {
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        //   products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.checkSubtotalPriceCartPage();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void can_select_free_shipping_radio_button_from_mycart_page() {
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        //   products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.isUpdateShoppingCartButtonVisibleAndHasTitle();
        myCart.clickOnUpdateShoppingCartButton();
        myCart.clickOnFreeShippingRadioOption();
        myCart.isUpdateTotalButtonVisibleAndHasTitle();
        myCart.clickOnUpdateTotalButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void can_select_flat_shipping_radio_button_from_mycart_page() {
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        //   products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        sleep(4000);
        myCart.isUpdateShoppingCartButtonVisibleAndHasTitle();
        myCart.clickOnUpdateShoppingCartButton();
        sleep(4000);
        myCart.clickOnFlateRateRadioOption();
        myCart.isUpdateTotalButtonVisibleAndHasTitle();
        myCart.clickOnUpdateTotalButton();
        while (magentoShopBasePage.verifyMiniHeaderCartIsEmpty()) {
            magentoShopBasePage.clickOnMiniHeaderCart();
            myCart.clickOnRemoveItemsFromMiniHeaderCart();
            myCart.clickOnCloseMiniHeaderView();
        }
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void can_add_product_level_three_in_mycart_page() {
        String subMenuIdentifier = "View All Electronics";
        String productIdentifier = "Large Camera Bag";
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelThree(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        //   products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
       while (magentoShopBasePage.verifyMiniHeaderCartIsEmpty()) {
            magentoShopBasePage.clickOnMiniHeaderCart();
            myCart.clickOnRemoveItemsFromMiniHeaderCart();
            myCart.clickOnCloseMiniHeaderView();
        }
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("MyCart")
    @Test
    public void can_add_two_products_in_mycart_page() {
        magentoShopBasePage.verifySelectLanguageTwo("English");
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo("Eyewear");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Gucci");
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.isContinueShoppingCartButtonVisibleAndHasTitle();
        myCart.clickOnContinueShoppingCartButton();
        products.clickOnHomePageLink();
        products.getSelectSpecificSubmenuProductsLevelTwo("Men");
        products.getSelectSpecificProductFromSpecificSubMenuProducts("Slim fit Dobby Oxford Shirt");
        products.fillInProductsQuantity("1");
        products.isShirtColorAndSizeVisible();
        products.clickOnShirtColor();
        products.clickOnShirtSize();
        products.clickOnAddToCartButton();
        sleep(4000);
        while (magentoShopBasePage.verifyMiniHeaderCartIsEmpty()) {
            magentoShopBasePage.clickOnMiniHeaderCart();
            myCart.clickOnRemoveItemsFromMiniHeaderCart();
            myCart.clickOnCloseMiniHeaderView();
        }
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }
}
