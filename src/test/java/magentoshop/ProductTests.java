package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("ProductTests")
public class ProductTests {
    MagentoShopBasePage magentoShopBasePage;
    Products products;
    Account account;
    Login login;

    @BeforeClass
    public void setup() {
        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("Products")
    @Test
    public void can_select_all_menu_products(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        products.getSelectAllMenuProducts();
    }

    @Feature("Products")
    @Test
    public void can_select_all_submenu_products(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        products.getSelectAllSubmenuProducts();
    }

    @Feature("Products")
    @Test
    public void can_get_child_element(){
        products = new Products();
        products.getChildElement();
    }

    @Feature("Products")
    @Test
    public void can_get_specific_menu_products(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Sale";
        products.getSelectSpecificMenuProducts(identifier.toUpperCase());
    }

    @Feature("Products")
    @Test
    public void can_get_specific_submenu_products_level_two(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Dress";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
    }

    @Feature("Products")
    @Test
    public void can_get_specific_submenu_products_level_three(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "View All Electronics";
        products.getSelectSpecificSubmenuProductsLevelThree(identifier);
    }

    @Feature("Products")
    @Test
    public void verify_dress_page_title(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Dress";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.isDressPageVisibleAndHasTitle();
    }

    @Feature("Products")
    @Test
    public void verify_dress_products_title(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Dress";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.isDressProductsVisibleAndHasTitle();
    }

    @Feature("Products")
    @Test
    public void can_select_dress_products(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Dress";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.clickOnDressRedProduct();
      // products.setZoomImage();
      //  products.setZoomWindowImage();

    }

    @Feature("Products")
    @Test
    public void verify_pearl_product_title_and_price(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.isJewelryPageVisibleAndHasTitle();
        products.isPearlStrandProductVisibleAndHasTextAndPrice();
    }

    @Feature("Products")
    @Test
    public void can_select_pearl_product_through_click_on_product(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.clickOnPearlStrandProduct();
    }

    @Feature("Products")
    @Test
    public void can_select_pearl_product_through_view_details(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.isPearlStrandProductViewDetailsVisibleAndHasTitle();
        products.clickOnPearlStrandProducViewDetails();
    }

    @Feature("Products")
    @Test
    public void verify_jewelry_page_sort_by_text(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.isJewelryPageSortByVisibleAndHasText();
        products.isJewelryPageSortBySelectionVisible();
        products.isJewelryPageSortSwitcherVisible();
        products.clickOnJewelryPageSortBySelection();
    }

    @Feature("Products")
    @Test
    public void sort_jewelry_page_by_name_ascending(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.sortJewelryPageByNameAscending();
    }

    @Feature("Products")
    @Test
    public void sort_jewelry_page_by_name_descending(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.sortJewelryPageByNameDescending();
    }

    @Feature("Products")
    @Test
    public void sort_jewelry_page_by_position_ascending(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.sortJewelryPageByPositionAscending();
    }

    @Feature("Products")
    @Test
    public void sort_jewelry_page_by_position_descending(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.sortJewelryPageByPositionDescending();
    }

    @Feature("Products")
    @Test
    public void sort_jewelry_page_by_price_ascending(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.sortJewelryPageByPriceAscending();
    }

    @Feature("Products")
    @Test
    public void sort_jewelry_page_by_price_descending(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.sortJewelryPageByPriceDescending();
    }

    @Feature("Products")
    @Test
    public void verify_pearl_strand_necklace_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.clickOnPearlStrandProduct();
        products.isPearlStrandNecklacePageVisible();
        products.isChooseAnOptionVisibleRequiredFieldAndHasText();
        products.isAddToCartButtonVisible();
        products.clickOnPearlStrandNecklaceTabs();
    }

    @Feature("Products")
    @Test
    public void can_go_to_jewelry_page_from_pearl_strand_necklace_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
      //  String identifier = "Jewelry";
      //  products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.isJewelryPageLinkVisibleAndHasText();
        products.clickOnJewelryPageLink();
    }

    @Feature("Products")
    @Test
    public void can_go_to_accessories_page_from_pearl_strand_necklace_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
     //   String identifier = "Jewelry";
     //   products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.isAccessoriesPageLinkVisibleAndHasText();
        products.clickOnAccessoriesPageLink();
    }

    @Feature("Products")
    @Test
    public void can_go_to_home_page_from_pearl_strand_necklace_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
       // String identifier = "Jewelry";
       // products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.isHomePageLinkVisibleAndHasText();
        products.clickOnHomePageLink();
    }

    @Feature("Products")
    @Test
    public void verify_add_to_cart_pearl_strand_necklace_product_when_required_field_is_empty(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
      //  products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.isAddToCartButtonVisible();
        products.clickOnAddToCartButton();
        products.isRequiredFieldVisibleAndHasText();
    }

    @Feature("Products")
    @Test
    public void can_add_to_cart_pearl_strand_necklace_product_when_required_field_is_not_empty(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        //products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.clickOnChooseAnOption();
        products.isProdusctsQuantityVisibleAndHasTitle();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
    }

    @Feature("Products")
    @Test
    public void verify_error_mesage_when_add_to_cart_non_existent_quantity_for_pearl_strand_necklace_product(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        //products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.clickOnChooseAnOption();
        products.isProdusctsQuantityVisibleAndHasTitle();
        products.fillInProductsQuantity("11");
        products.clickOnAddToCartButton();
        products.isQuantityErrorMessageVisibleAndHasText();
    }

    @Feature("Products")
    @Test //Aici trebuie ridicat defect; se poate introduce si blank
    public void verify_error_mesage_when_add_to_cart_incorrect_quantity_for_pearl_strand_necklace_product(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String identifier = "Jewelry";
        products.getSelectSpecificSubmenuProductsLevelTwo(identifier);
        //products.selectSubmenuJewelryProducts();
        products.clickOnPearlStrandProduct();
        products.clickOnChooseAnOption();
        products.isProdusctsQuantityVisibleAndHasTitle();
        products.fillInProductsQuantity("XX");
        products.clickOnAddToCartButton();
        products.isQuantityErrorMessageVisibleAndHasText();
    }

    @Feature("Products")
    @Test
    public void can_select_specific_products_from_specific_submenu_products(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        products = new Products();
        String subMenuIdentifier = "Jewelry";
        String productIdentifier = "Pearl Strand Necklace";
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        products.clickOnChooseAnOption();
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
    }

}
