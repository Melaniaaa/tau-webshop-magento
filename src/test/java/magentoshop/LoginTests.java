package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Selenide.sleep;

@Epic("LoginTests")
public class LoginTests {
    MagentoShopBasePage magentoShopBasePage;
    Account account;
    Login login;

    @BeforeClass
    public void setup() {

        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("Login")
    @Test
    public void verify_account_login_page_title() {
        String loginText = "Login or Create an Account";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isLoginPageVisibleAndHasLoginText(loginText);
    }

    @Feature("Login")
    @Test
    public void verify_account_login_new_users_contents() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isNewUsersFromLoginPageVisibleAndHasContents();
    }

    @Feature("Login")
    @Test
    public void can_open_create_an_account_from_login_page() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.createAnAccountFromLoginPage();
    }

    @Feature("Login")
    @Test
    public void verify_account_login_already_registered_contents() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isAlreadyRegisteredFromLoginPageVisibleAndHasContents();
    }

    @Feature("Login")
    @Test
    public void verify_if_email_address_is_required_field() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isEmailAddressRequiredField();
    }

    @Feature("Login")
    @Test
    public void verify_if_password_is_required_field() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isPasswordRequiredField();
    }

    @Feature("Login")
    @Test
    public void can_open_forgot_password_page() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isForgotPasswordVisible();
        login.clickOnForgotPassword();
        login.isBackToLoginVisible();
        login.clickOnBackToLogin();
    }

    @Feature("Login")
    @Test
    public void can_login_with_valid_credentials() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isEmailAddressVisible();
        login.fillInEmailAddress("TLM@gmail.com");
        login.isPasswordVisible();
        login.fillInPassword("leti03");
        login.isLoginButtonVisible();
        login.isLoginButtonEnable();
        login.clickOnLoginButton();
        String welcomeText = "Welcome, Letitia-Melania Tusa!";
        magentoShopBasePage.verifyWelcomeMessageByText(welcomeText);
        magentoShopBasePage.clickAccountList();
        account.clickLogOut();
    }

    @Feature("Login")
    @Test
    public void wrong_password_throws_error_message() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.isPasswordVisible();
        login.fillInPassword("leti09");
        login.clickOnLoginButton();
        login.wrongPassword();
    }

    @Feature("Login")
    @Test
    public void wrong_email_throws_error_message() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isEmailAddressVisible();
        login.fillInEmailAddress("TLM@gmail");
        login.isPasswordVisible();
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        login.wrongEmail();
    }

    @Feature("Login")
    @Test
    public void verify_login_with_blank_all_credentials() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.clickOnLoginButton();
        login.isValidationBlankEmailAddress();
        login.isValidationBlankPassword();
    }

    @Feature("Login")
    @Test
    public void verify_login_with_blank_email_address() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isPasswordVisible();
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        login.isValidationBlankEmailAddress();
    }

    @Feature("Login")
    @Test
    public void verify_login_with_blank_password() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isEmailAddressVisible();
        login.fillInEmailAddress("TLM@gmail.com");
        login.clickOnLoginButton();
        login.isValidationBlankPassword();
    }

    @Feature("Login")
    @Test
    public void verify_my_dashboard_page_after_succesful_login() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isEmailAddressVisible();
        login.fillInEmailAddress("TLM@gmail.com");
        login.isPasswordVisible();
        login.fillInPassword("leti03");
        login.isLoginButtonVisible();
        login.isLoginButtonEnable();
        login.clickOnLoginButton();
        login.isMyDashboardVisibleAndHasText();
        login.isFirstWelcomeMessageVisibleAndHasText();
        login.isSecondWelcomeMessageVisibleAndHasText();
        magentoShopBasePage.clickAccountList();
        account.clickLogOut();
    }

    @Feature("Login")
    @Test
    public void verify_logout_from_aplication() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.isEmailAddressVisible();
        login.fillInEmailAddress("TLM@gmail.com");
        login.isPasswordVisible();
        login.fillInPassword("leti03");
        login.isLoginButtonVisible();
        login.isLoginButtonEnable();
        login.clickOnLoginButton();
        magentoShopBasePage.clickAccountList();
        account.isLogoutVisibleAndHasText();
        account.clickLogOut();
    }
}