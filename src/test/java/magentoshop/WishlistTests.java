package magentoshop;

import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

public class WishlistTests {
    MagentoShopBasePage magentoShopBasePage;
    Products products;
    Account account;
    Login login;
    Wishlist wishlist;
    MyCart myCart;

    @BeforeClass
    public void setup() {
        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("Wishlist")
    @Test
    public void can_add_products_to_wishlist(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        products.fillInProductsQuantity("1");
        wishlist = new Wishlist();
        wishlist.isAddToWishlistLinkVisibleAndHasText();
        wishlist.clickOnAddToWishlistLink();
        magentoShopBasePage.openAccountList();
        account.openMyWhislist();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Wishlist")
    @Test
    public void can_move_products_to_wishlist_from_cart_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        wishlist = new Wishlist();
        wishlist.isMoveToWishlistVisibleAndHasText();
        wishlist.clickOnMoveToWishlistLink();
        magentoShopBasePage.openAccountList();
        account.openMyWhislist();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Wishlist")
    @Test
    public void can_fill_text_area_from_wishlist_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        wishlist = new Wishlist();
        wishlist.isMoveToWishlistVisibleAndHasText();
        wishlist.clickOnMoveToWishlistLink();
        magentoShopBasePage.openAccountList();
        account.openMyWhislist();
        wishlist.isTextAreaVisible();
        wishlist.fillInTextArea("These are my favorite glasses!");
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Wishlist")
    @Test
    public void can_update_wishlist_from_wishlist_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        wishlist = new Wishlist();
        wishlist.isMoveToWishlistVisibleAndHasText();
        wishlist.clickOnMoveToWishlistLink();
        magentoShopBasePage.openAccountList();
        account.openMyWhislist();
        wishlist.isTextAreaVisible();
        wishlist.fillInTextArea("These are my favorite glasses!");
        wishlist.isUpdateWishlistVisibleAndHasTitle();
        wishlist.clickOnUpdateWishlistButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }

    @Feature("Wishlist")
    @Test
    public void can_delete_products_from_wishlist_page(){
        magentoShopBasePage.verifySelectLanguageTwo("English");
        String subMenuIdentifier = "Eyewear";
        String productIdentifier = "Gucci";
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
        login = new Login();
        login.fillInEmailAddress("TLM@gmail.com");
        login.fillInPassword("leti03");
        login.clickOnLoginButton();
        products = new Products();
        products.getSelectSpecificSubmenuProductsLevelTwo(subMenuIdentifier);
        products.getSelectSpecificProductFromSpecificSubMenuProducts(productIdentifier);
        products.fillInProductsQuantity("1");
        products.clickOnAddToCartButton();
        myCart = new MyCart();
        myCart.clickOnUpdateShoppingCartButton();
        wishlist = new Wishlist();
        wishlist.isMoveToWishlistVisibleAndHasText();
        wishlist.clickOnMoveToWishlistLink();
        magentoShopBasePage.openAccountList();
        account.openMyWhislist();
        wishlist.isTextAreaVisible();
        wishlist.fillInTextArea("These are my favorite glasses!");
        wishlist.isUpdateWishlistVisibleAndHasTitle();
        wishlist.clickOnUpdateWishlistButton();
        wishlist.isDeleteWishlistButtonVisibleAndHasTitle();
        wishlist.clickOnDeleteWishlistButton();
        magentoShopBasePage.openAccountList();
        account.clickLogOut();
    }
}
