package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("MagentoShopHomePageTests")
public class MagentoShopBasePageTests {
    MagentoShopBasePage magentoShopBasePage;

    @BeforeClass
      public void setup() {
         magentoShopBasePage = new MagentoShopBasePage();
      }

    @Feature("HomePage")
    @Test(description = "I am expecting a welcome message here.")
    public void can_expect_a_welcome_message() {
        String welcomeText = "Welcome";
        magentoShopBasePage.verifyWelcomeMessageByText(welcomeText);
      //  demoShopBasePage.resetPage();
    }

    @Feature("HomePage")
    @Test(description = "I am expecting a logo message here.")
    public void can_expect_a_logo_message(){
        String logoText = "Madison Island";
        magentoShopBasePage.verifyLogoMessage(logoText);
    }

    @Feature("HomePage")
    @Test
    public void language_text(){
        String languageText = "Your Language";
        magentoShopBasePage.verifyLabelFormLanguage(languageText);
    }

    @Feature("HomePage")
    @Test(description = "I expect the language chosen here to be selected. " +
            "Meaning English.")
    public void can_select_english_language(){
        String language = "English";
        magentoShopBasePage.verifySelectLanguage(language);
    }

    @Feature("HomePage")
    @Test(description = "I expect the language chosen here to be selected. " +
            "Meaning French.")
    public void can_select_french_language(){
        String language = "French";
        magentoShopBasePage.verifySelectLanguage(language);
    }

    @Feature("HomePage")
    @Test(description = "I expect the language chosen here to be selected. " +
            "Meaning German.")
    public void can_select_german_language(){
        String language = "German";
        magentoShopBasePage.verifySelectLanguage(language);
    }

    @Feature("HomePage")
    @Test(description = "I expect the language chosen here to be selected. " +
            "Meaning English.")
    public void can_select_english_language_second_method(){
        String languageTwo = "English";
        magentoShopBasePage.verifySelectLanguageTwo(languageTwo);
    }

    @Feature("HomePage")
    @Test(description = "I expect the language chosen here to be selected. " +
            "Meaning French.")
    public void can_select_french_language_second_method(){
        String languageTwo = "French";
        magentoShopBasePage.verifySelectLanguageTwo(languageTwo);
    }

    @Feature("HomePage")
    @Test(description = "I expect the language chosen here to be selected. " +
            "Meaning German.")
    public void can_select_german_language_second_method(){
        String languageTwo = "German";
        magentoShopBasePage.verifySelectLanguageTwo(languageTwo);
    }

   @Feature("HomePage")
   @Test(description = "I expect to be able to select the desired product here.")
    public void can_search_any_products(){
        String inputSearch = "jewelry";
        //String inputSearch = "shirt";
        magentoShopBasePage.verifySearch(inputSearch);
    }

    @Feature("HomePage")
    @Test(description = "I expect to be able to open the list of options here.")
    public void can_open_account_list(){
        magentoShopBasePage.openAccountList();
    }

    @Feature("HomePage")
    @Test
    public void title_account_list(){
        magentoShopBasePage.verifyAccountList();
    }

    @Feature("HomePage")
    @Test
    public void verify_mini_header_basket (){
        magentoShopBasePage.verifyMiniHeaderCart();
    }

    @Feature("HomePage")
    @Test(description = "I expect to see the content to mini header cart.")
    public void can_view_the_contents_of_mini_header_basket (){
        magentoShopBasePage.clickOnMiniHeaderCart();
    }
}
