package magentoshop;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("AccountTests")
public class AccountTests {
    MagentoShopBasePage magentoShopBasePage;
    Account account;

    @BeforeClass
    public void setup() {
        magentoShopBasePage = new MagentoShopBasePage();
    }

    @Feature("Account")
    @Test(description = "I expect to see the title of the MyAccount page.")
    public void verify_my_account_title_text(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.isMyAccountVisibleAndHasTitleText();
        magentoShopBasePage.clickAccountList();
    }

    @Feature("Account")
    @Test(description = "I expect to can open MyAccount page.")
    public void can_open_my_account_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyAccount();
       // magentoShopBasePage.closeAccountList();

    }

    @Feature("Account")
    @Test(description = "I expect to see the title of the MyAccount page.")
       public void verify_my_whislist_title_text(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.isMyWhislistVisibleAndHasTitleText();
        magentoShopBasePage.clickAccountList();
    }

    @Feature("Account")
    @Test(description = "I expect to can open Whislist page.")
    public void can_open_my_whislist_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyWhislist();
      //  sleep(6000);
       // magentoShopBasePage.closeAccountList();
    }

    @Feature("Account")
    @Test(description = "I expect to see the title of the MyCart page.")
    public void verify_my_cart_title_text(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.isMyCartVisibleAndHasTitleText();
        magentoShopBasePage.clickAccountList();
    }

    @Feature("Account")
    @Test(description = "I expect to can open MyCart page.")
    public void can_open_my_cart_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openMyCart();
    }

    @Feature("Account")
    @Test(description = "I expect to see the title of the Checkout page.")
    public void verify_checkout_title_text() {
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.isCheckoutVisibleAndHasText();
        magentoShopBasePage.clickAccountList();;
    }

    @Feature("Account")
    @Test(description = "I expect to can open Checkout page.")
    public void can_open_checkout_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openCheckout();
    }

    @Feature("Account")
    @Test(description = "I expect to see the title of the Login page.")
    public void verify_login_title_text(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.isLoginVisibleAndHasText();
        magentoShopBasePage.clickAccountList();
    }

    @Feature("Account")
    @Test(description = "I expect to can open Login page.")
    public void can_open_login_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openLogin();
    }

    @Feature("Account")
    @Test(description = "I expect to see the title of the Register page.")
    public void verify_register_title_text(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.isRegisterVisibleAndHasTitle();
      //  magentoShopBasePage.clickAccountList();
    }

    @Feature("Account")
    @Test(description = "I expect to can open Register page.")
    public void can_open_register_page(){
        magentoShopBasePage.openAccountList();
        account = new Account();
        account.openRegister();
    }
}
